# ShURL

This project is a URL shortener service written in Go. It provides an API and a
simple client based on it to shorten URLs and stores the shortened URLs in a
SQLite database powered by [Turso](https://turso.tech/).

## Environment Variables

Create a `.env` file in the root of your project and set the following
environment variables:

```dotenv
DB_URL="libsql://<your turso db name>.turso.io?authToken=<your db auth token>"

CACHE_MAX_ITEMS_COUNT=65536 # 2 ^ 16 # int

GENERATOR_CHARSET="abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789" # any charset you desire
GENERATOR_HASH_LENGTH=7 # uint 8
```

- `DB_URL`: The connection URL for your Turso database, including the
  authentication token.
- `CACHE_MAX_ITEMS_COUNT`: The maximum number of items to store in the cache.
- `GENERATOR_CHARSET`: The character set used to generate the shortened URL
  hashes.
- `GENERATOR_HASH_LENGTH`: The length of the generated hash for the shortened
  URLs.

## Database Schema

The URL shortener service uses a SQLite database. Ensure the following table is
created in your database:

```sql
CREATE TABLE IF NOT EXISTS
  urls (
    hash TEXT PRIMARY KEY,
    url TEXT NOT NULL,
    createdAt DATETIME DEFAULT CURRENT_TIMESTAMP
  );
```

This table stores the shortened URLs with the following fields:

- `hash`: The unique hash for the shortened URL (primary key).
- `url`: The original URL.
- `createdAt`: The timestamp when the URL was created.

## Installation

1. Clone the repository:
   ```bash
   git clone https://gitlab.com/sudoCss/shurl.git
   cd shurl
   ```

2. Install dependencies:
   ```bash
   go mod tidy
   ```

3. Set up the environment variables in a `.env` file as described above.

4. Initialize the database schema by running the following SQL command in your
   SQLite database:
   ```sql
   CREATE TABLE IF NOT EXISTS
     urls (
       hash TEXT PRIMARY KEY,
       url TEXT NOT NULL,
       createdAt DATETIME DEFAULT CURRENT_TIMESTAMP
     );
   ```

## Usage

To start the server, run:

```bash
go run cmd/server/main.go
```

The server will start and listen on the port 8080. You can send POST requests to
shorten URLs and GET requests to retrieve the original URLs.

### Example CURL Command to Shorten a URL

```bash
curl -X POST -H "Content-Type: application/json" -d '{"url": "https://example.com"}' http://localhost:8080/api/v1/shorten
```

### Example CURL Command to Redirect to the Original URL

```bash
curl -X GET http://localhost:8080/r/<shortened-hash>
```

or open the the url in the browser `http://localhost:8080/r/<shortened-hash>`

## Contributing

If you would like to contribute to this project, please fork the repository and
submit a merge request. I welcome all contributions!

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file
for details.
