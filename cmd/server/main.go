package main

import (
	"fmt"
	"log"
	"net/url"
	"os"
	"strconv"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/fiber/v2/middleware/limiter"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/fiber/v2/middleware/monitor"
	"github.com/gofiber/fiber/v2/middleware/recover"
	"github.com/joho/godotenv"
	"gitlab.com/sudoCss/shurl/pkg/cache"
	"gitlab.com/sudoCss/shurl/pkg/db"
	"gitlab.com/sudoCss/shurl/pkg/generator"
)

type ShURL struct {
	app       *fiber.App
	db        *db.DB
	cache     *cache.Cache
	generator *generator.Generator
}

type ShortenRequestBody struct {
	URL string `json:"url"`
}

var shurl ShURL

func init() {
	if os.Getenv("CURRENT_ENV") != "prod" {
		err := godotenv.Load()
		if err != nil {
			log.Fatal("Error loading .env file")
		}
	}

	if os.Getenv("DB_URL") == "" || os.Getenv("CACHE_MAX_ITEMS_COUNT") == "" || os.Getenv("GENERATOR_CHARSET") == "" || os.Getenv("GENERATOR_HASH_LENGTH") == "" {
		log.Fatal("Missing some required environment variables, see README.md")
	}

	maxCacheItems, err := strconv.Atoi(os.Getenv("CACHE_MAX_ITEMS_COUNT"))
	if err != nil {
		log.Fatal("CACHE_MAX_ITEMS_COUNT should be a number and fit in int")
	}

	hashLength, err := strconv.Atoi(os.Getenv("GENERATOR_HASH_LENGTH"))
	if err != nil || hashLength > 255 {
		log.Fatal("GENERATOR_HASH_LENGTH should be a number and fit in uint8")
	}

	shurl = ShURL{
		app: fiber.New(fiber.Config{
			CaseSensitive: true,
			AppName:       "ShURL",
		}),
		db:        db.NewDB(os.Getenv("DB_URL")),
		cache:     cache.NewCache(maxCacheItems),
		generator: generator.NewGenerator(os.Getenv("GENERATOR_CHARSET"), uint8(hashLength)),
	}
}

func main() {
	defer shurl.db.Close()

	shurl.app.Use(logger.New(logger.Config{
		Format: "${time} | ${status} | ${latency} | ${ip}:${port} | ${method} | ${path} | ${error}\n",
	}))
	shurl.app.Use(recover.New())
	shurl.app.Use(cors.New())
	shurl.app.Use(limiter.New(limiter.Config{
		Max: 15,
	}))
	shurl.app.Get("/metrics", monitor.New())

	shurl.app.Static("/", "./static")

	shurl.app.Route("/api", func(api fiber.Router) {
		api.Route("/v1", func(v1 fiber.Router) {
			v1.Post("/shorten", handleShortenURL)
		})
	})

	shurl.app.Get("/r/:hash", handleRedirectToOriginalURL)

	log.Fatal(shurl.app.Listen(":8080"))
}

func handleShortenURL(c *fiber.Ctx) error {
	var body ShortenRequestBody

	if err := c.BodyParser(&body); err != nil {
		return c.Status(fiber.StatusBadRequest).JSON(&fiber.Map{
			"success": false,
			"message": "Bad request.",
			"data":    &fiber.Map{},
		})
	}

	if body.URL == "" {
		return c.Status(fiber.StatusBadRequest).JSON(&fiber.Map{
			"success": false,
			"message": "The url field is required.",
			"data":    &fiber.Map{},
		})
	}

	if ok := isValidURL(body.URL); !ok {
		return c.Status(fiber.StatusBadRequest).JSON(&fiber.Map{
			"success": false,
			"message": "A valid URL is required.",
			"data":    &fiber.Map{},
		})
	}

	for i := range 10 {
		hash, err := shurl.generator.Generate()
		if err != nil {
			return c.Status(fiber.StatusInternalServerError).JSON(&fiber.Map{
				"success": false,
				"message": "Something went wrong on the server, try again later or contact us.",
				"data":    &fiber.Map{},
			})
		}

		if err := shurl.db.Insert(hash, body.URL); err != nil {
			if i == 9 {
				return c.Status(fiber.StatusInternalServerError).JSON(&fiber.Map{
					"success": false,
					"message": "Something went wrong on the server, try again later or contact us.",
					"data":    &fiber.Map{},
				})
			}

			continue
		}

		shurl.cache.Insert(hash, body.URL)

		return c.Status(fiber.StatusCreated).JSON(&fiber.Map{
			"success": true,
			"message": "Short url created successfully.",
			"data": &fiber.Map{
				"shortURL": fmt.Sprintf("%s/r/%s", c.BaseURL(), hash),
			},
		})
	}

	return c.Status(fiber.StatusInternalServerError).JSON(&fiber.Map{
		"success": false,
		"message": "We don't know what happened, try again later or contact us.",
		"data":    &fiber.Map{},
	})
}

func handleRedirectToOriginalURL(c *fiber.Ctx) error {
	hash := c.Params("hash")

	originalURL, err := shurl.cache.Lookup(hash)
	if err != nil {
		url, err := shurl.db.Query(hash)
		if err != nil {
			return c.Status(fiber.StatusNotFound).JSON(&fiber.Map{
				"success": false,
				"message": "Not a valid url",
				"data":    &fiber.Map{},
			})
		}

		originalURL = url.URL
	}

	shurl.cache.Insert(hash, originalURL)
	return c.Redirect(originalURL, fiber.StatusPermanentRedirect)
}

func isValidURL(str string) bool {
	u, err := url.Parse(str)
	return err == nil && u.Scheme != "" && u.Host != ""
}
