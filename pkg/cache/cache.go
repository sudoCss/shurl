package cache

import (
	"fmt"
	"slices"
	"sync"
	"time"
)

type cacheItem struct {
	hash           string
	url            string
	insertDate     time.Time
	lastLookupDate time.Time
	lookupsCount   int64
}

type Cache struct {
	cache        map[string]cacheItem
	lock         sync.RWMutex
	maxCacheSize int
}

func NewCache(maxCacheSize int) *Cache {
	return &Cache{
		cache:        make(map[string]cacheItem),
		maxCacheSize: maxCacheSize,
	}
}

func (c *Cache) Insert(key, targetURL string) {
	c.lock.Lock()
	defer c.lock.Unlock()

	c.cache[key] = cacheItem{
		hash:           key,
		url:            targetURL,
		insertDate:     time.Now(),
		lastLookupDate: time.Now(),
		lookupsCount:   0,
	}

	if len(c.cache) > c.maxCacheSize {
		c.refresh(c.maxCacheSize / 2)
	}
}

func (c *Cache) Lookup(key string) (string, error) {
	c.lock.RLock()
	defer c.lock.RUnlock()

	val, ok := c.cache[key]
	if !ok {
		return "", fmt.Errorf("cache miss: key %s isn't present in cache", key)
	}

	val.lastLookupDate = time.Now()
	val.lookupsCount++

	return val.url, nil
}

func (c *Cache) Clear() {
	for k := range c.cache {
		delete(c.cache, k)
	}
}

func (c *Cache) refresh(targetSize int) {
	if ok := c.lock.TryLock(); ok {
		defer c.lock.Unlock()
	}

	itemsToRemove := make([]cacheItem, 0, len(c.cache))

	for _, v := range c.cache {
		itemsToRemove = append(itemsToRemove, v)
	}

	slices.SortFunc(itemsToRemove, func(a, b cacheItem) int {
		cmpRes := a.lastLookupDate.Compare(b.lastLookupDate)

		if cmpRes != 0 {
			return cmpRes
		}

		if a.lookupsCount < b.lookupsCount {
			return -1
		} else if a.lookupsCount > b.lookupsCount {
			return 1
		}

		return 0
	})

	itemsToRemoveCount := len(c.cache) - targetSize
	for i := 0; i < itemsToRemoveCount; i++ {
		delete(c.cache, itemsToRemove[i].hash)
	}
}
