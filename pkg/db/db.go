package db

import (
	"database/sql"
	"fmt"
	"os"
	"time"

	_ "github.com/tursodatabase/libsql-client-go/libsql"
)

type DB struct {
	instance *sql.DB
}

type URL struct {
	Hash      string
	URL       string
	CreatedAt time.Time
}

func NewDB(url string) *DB {
	db, err := sql.Open("libsql", url)
	if err != nil {
		fmt.Fprintf(os.Stderr, "failed to open db %s: %s", url, err)
		os.Exit(1)
	}

	return &DB{
		instance: db,
	}
}

func (db *DB) Query(hash string) (*URL, error) {
	query := `SELECT hash, url, createdAt FROM urls WHERE hash = ?`
	row := db.instance.QueryRow(query, hash)

	var url URL
	if err := row.Scan(&url.Hash, &url.URL, &url.CreatedAt); err != nil {
		if err == sql.ErrNoRows {
			return nil, fmt.Errorf("format string %s", err)
		}
		return nil, fmt.Errorf("format string %s", err)
	}

	return &url, nil
}

func (db *DB) Insert(hash, url string) error {
	_, err := db.instance.Exec("INSERT INTO urls (hash, url) VALUES (?, ?)", hash, url)
	if err != nil {
		return err
	}

	return nil
}

func (db *DB) Close() {
	db.instance.Close()
}
