package generator_test

import (
	"strings"
	"testing"

	"gitlab.com/sudoCss/shurl/pkg/generator"
)

const (
	charset    = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	hashLength = 7
)

func TestGenerate(t *testing.T) {
	g := generator.NewGenerator(charset, hashLength)
	hash, err := g.Generate()
	if err != nil {
		t.Fatalf("%v", err)
	}

	if len(hash) != hashLength {
		t.Errorf("expected hash length %d, got %d", hashLength, len(hash))
	}

	for _, char := range hash {
		if !strings.Contains(charset, string(char)) {
			t.Errorf("hash contains invalid character %c", char)
		}
	}
}

func TestGenerateCollision(t *testing.T) {
	// IMPORTANT: Not a good test in general, but wanted to see the possibility of collisions

	g := generator.NewGenerator(charset, hashLength)
	hashes := make(map[string]bool, 0)

	for i := 0; i < 1000_000; i++ {
		hash, err := g.Generate()
		if err != nil {
			t.Fatalf("expected no error, got %v", err)
		}

		if ok := hashes[hash]; ok {
			t.Fatalf("expected no collision, got hash collision o %s", hash)
		}

		hashes[hash] = true
	}
}
