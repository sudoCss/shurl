package generator

import (
	"crypto/rand"
)

type Generator struct {
	charset    string
	hashLength uint8
}

func NewGenerator(charset string, hashLength uint8) *Generator {
	return &Generator{
		charset:    charset,
		hashLength: hashLength,
	}
}

func (g *Generator) Generate() (string, error) {
	randBytes := make([]byte, g.hashLength)
	_, err := rand.Read(randBytes)
	if err != nil {
		return "", err
	}

	for i, b := range randBytes {
		randBytes[i] = g.charset[b%byte(len(g.charset))]
	}

	hash := string(randBytes)

	return hash, nil
}
